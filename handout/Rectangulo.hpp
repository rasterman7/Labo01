#ifndef RECTANGULO_HPP_
#define RECTANGULO_HPP_

#include "Par.hpp"
#include <iostream>

namespace algo2
{

class Rectangulo
{
  public:

    Rectangulo(const Par& origen, const Par& tamanio);

    const Par& origen() const;

    const Par& tamanio() const;

    float area() const;
    
    friend std::ostream& operator<< (std::ostream& os, const Rectangulo& r);
  
  private:
	Par _origen;
	Par _tamanio;
};

Rectangulo::Rectangulo(const Par& origen, const Par& tamanio): _origen(origen), _tamanio(tamanio){}

const Par& Rectangulo::origen() const{
	return _origen;
}

const Par& Rectangulo::tamanio() const{
	return _tamanio;
}

float Rectangulo::area() const{
	return (this->tamanio().x - this->origen().x)*(this->tamanio().y - this->origen().y);
}

std::ostream& operator<< (std::ostream& os, const Rectangulo& r){
	os << r.origen() << r.tamanio() << r.area();
	return os;
}

} // algo2

#endif // RECTANGULO_HPP_

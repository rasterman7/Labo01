#ifndef PAR_HPP_
#define PAR_HPP_

#include <iostream>

namespace algo2
{

struct Par
{
	Par(int a, int b);
	Par(const Par& p);
	int x, y;
	friend std::ostream& operator << (std::ostream& os, const algo2::Par& p)
	{	
		os << p.x << p.y;
		return os;
	}
};

Par::Par(int a, int b): x(a), y(b){}

Par::Par(const Par& p){
	x = p.x;
	y = p.y;
}

} // algo2
#endif // PAR_HPP_
